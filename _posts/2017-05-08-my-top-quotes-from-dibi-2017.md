---
title: My top quotes from DiBi 2017
published: true
---
I had the pleasure to attend this years Design It Build It conference in Edinburgh. It was my first UX conference, so I didn't really know what I could expect from it. The two days were challenging and in some parts changed my mind. I want to share some of my take aways with you.

The conf started with a Keynote hold by [Joshua Davis][joshua-davis]. He opened my eyes on how to handle open source stuff. He writes animation software in Java. You probably saw these animations in some of the last years SuperBowl pre shows.

> Don't overthink things. Just put it out. <br /> Joshua Davis

Don't overthink things, was one quote from this session that I hear in my mind for weeks now. Thats why I want to write more on this blog. ;)

> The kind of work you do is the kind of work you get hired to do. <br /> Joshua Davis

Sometimes it is that simple. People know things they see. Even in the digital business people can only guess what your skills are. Maybe from some kind of skill list, but better with results (or intermediates). When I want to do more UX work and get payed for it - it should do more UX stuff. Sometimes it is that simple.

> Ads are not the problem. Trackers are the problem <br /> Laura Kalbag

[Lauras][laura-kalbag] talk was about.. Call it trackers. Call it data. Call it the black site of the digital industry. It felt like Big Brother. She showed what data collecting companies can do with your (their?) data. And how should not do that. She indroduced the [Ethical Design Manifesto](https://ind.ie/ethical-design/). A good guideline for companies and individuals that worry about the users data.

> Shit or get of the pott <br /> Christopher Murphy

Chris startet his talk with a story, when he ended up in the ambulance. The baseline of that talk was: You have everything you need. You are good. Start living your dream. Do the thing you want to do. Dont only think about what you could do. Do it! His talk reminded me of Eugene Cho's book [Overrated][overrated-book].

> Use the fuck off phase. <br /> Chris Murphy

Yet another quote from Chris. He mentioned some phases when one has to do with deadlines. In the first place one probably has plenty of time. The deadline is far away, so there is no need to start implementing. Things get deplayed in the first place. He called this first phase the *Fuck Off Phase*. A phase, where you have all the time. All the time to procastinate and indirectly think about your ideas. The time to sharpen the saw.

> Trade everything as a prototype <br /> Chris Hammond

Chris Hammond introduced how IBM does Design Thinking. One sentence mad his way into my brain and lives there. They treat everything as a prototype. Nothing is cut into stone - not even live environments. So it should be easy to rip off things and replace them with a new - even better one. A great attitude and I want more of this.

> Design Thinking is basically to rephrase the problem <br /> Chris Hammond

This was the first thing I wrote down when Chris started his talk. He put an exercise at the beginning, asked for some volunteers and asked them to draw a vase. And everyone drawed a vase. Then they should get rid of this prototype. And create a stunning experience to enjoy flowers. This blew my mind. Just by using other words.

> Words matter! <br /> Marcel Henning

Thats not a quote but [a learning][words-matter] from [Louise Mushet's][louise-mushet] talk. She talked about the change she could establish in a charity. So when she was about to do some user research, people stuck and didnt want to talk. Instead of using the word "research" she used "insights". And with that, people stated to talk with her.

DiBi 2017 was a changing experience. I can recommend it to everyone doing some tech stuff, not just designers.

[overrated-book]: https://twitter.com/EugeneCho/status/506651113999699968
[joshua-davis]: https://twitter.com/joshuadavis
[laura-kalbag]: https://twitter.com/laurakalbag
[louise-mushet]: https://twitter.com/louisemushet
[words-matter]: https://twitter.com/moerssl/status/847816440744902660
