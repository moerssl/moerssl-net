---
layout: single
title: English, Jekyll and my way away from WordPress
status: publish
published: true
date: '2016-04-03 13:05:45 +0200'
date_gmt: '2016-04-03 11:05:45 +0200'
categories:
  - News
---
This blog has a fresh look and from now on I will write my posts in English. The reason is very simple. Most of my twitter followers dont speak german and I want you to understand me.

So there is a new design and a new language. And to get to three new things in a row, I changed my system from WordPress to Jekyll. I liked WordPress for years and I did several pages with this system. BUT I dont want all the administration hassle that comes with a wide spread system. WordPress is one of the most often used systems on the planet - and so it is vulnerable when you dont keep track with all the security updates.

In my sparetime when I just want to write some posts it is not in my interest to update my site within days when a new update is out. So I switched to a static site generator called Jekyll. Now it's easy for me to write post. I just open up my favorite text editor - and start typing. No editing in a browser, no shortcuts I dont know and no mouse when I dont need it. And - Markdown! Now I can write and let some special characters do the formatting.

So I hope this will result in some more posts than in the past years.
