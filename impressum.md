---
layout: single
title: Impressum
header:
  overlay_image: /assets/images/twitter.jpg
---

#### Angaben gemäß § 5 TMG:

Marcel Henning <br />
Wecholder Straße 164 <br />
28277 Bremen

#### Kontakt:

E-Mail: blog@moerssl.net

#### Verantwortlich für den Inhalt nach § 55 Abs. 2 RStV:

Marcel Henning <br />
Wecholder Straße 164 <br />
28277 Bremen

Quelle: <http://www.e-recht24.de>
